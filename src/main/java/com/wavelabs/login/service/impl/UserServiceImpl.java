package com.wavelabs.login.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.login.repositories.UserRepository;
import com.wavelabs.login.services.UserService;
import com.wavelabs.login.user.User;

@Service
public class UserServiceImpl implements UserService {
	static Logger log = Logger.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepository userRepo;

	@Override
	public boolean createNewUser(User user) {
		try {
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

	@Override
	public User getProfile(Integer userId) {
		try {

			User user = userRepo.findById(userId);
			return user;
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}

	/*@Override
	public User getMobileFromEmail(String userEmail) {
		try {

			User user = userRepo.findMobileByEmail(userEmail);
			System.out.println(user);
			System.out.println(user.getMobile());
			return user;
		} catch (NullPointerException e) {
			log.error(e);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			return null;
		}
	}*/



	@Override
	public User checkMobileInDB(String userEmail, String mobile) {
		try{
			User user = userRepo.findByEmailAndMobile(userEmail,mobile);
			System.out.println(user.getMobile());
			System.out.println(user.getEmail());
			return user;
		} catch (NullPointerException e) {
			log.error(e);
			//System.out.println(e.getMessage());
			//System.out.println(e.getCause());
			return null;
		}	
	}
}
